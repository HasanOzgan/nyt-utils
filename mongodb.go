package utils

import (
	"encoding/json"
	"labix.org/v2/mgo"
)

func Insert(uri string, dbName string, documentName string, data string) error {
	session, err := mgo.Dial(uri)
	if err != nil {
		panic(err)
	}

	defer session.Close()

	session.SetMode(mgo.Monotonic, true)
	c := session.DB(dbName).C(documentName)

	// Insert Datas
	var item interface{}
	json.Unmarshal([]byte(data), &item)

	err = c.Insert(item)
	if err != nil {
		return err
	}

	return nil
}

func InsertAll(uri string, dbName string, documentName string, data string) (int, error) {
	count := 0
	session, err := mgo.Dial(uri)
	if err != nil {
		panic(err)
	}

	defer session.Close()

	session.SetMode(mgo.Monotonic, true)
	c := session.DB(dbName).C(documentName)

	// Insert Datas
	var items []interface{}
	err = json.Unmarshal([]byte(data), &items)
	if err != nil {
		return 0, err
	}
	count = len(items)
	err = c.Insert(items...)

	return count, nil
}
