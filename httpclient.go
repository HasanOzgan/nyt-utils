package utils

import (
	// Things needed by the actual interface.
	"golang.org/x/net/proxy"
	"net/http"
	"net/url"

	// Things needed by the example code.
	"fmt"
	"io/ioutil"
	"os"
  "strings"
  "regexp"
)

type MyHttpClient struct {
  client *http.Client
  headers map[string]string
}

func fatalf(fmtStr string, args interface{}) {
	fmt.Fprintf(os.Stderr, fmtStr, args)
	os.Exit(-1)
}

func NewClient() *MyHttpClient{
  client := &http.Client{}

  return &MyHttpClient{client, map[string]string{}}
}

func NewClientWithProxy(proxyURI string) *MyHttpClient{
  // Create a transport that uses Tor Browser's SocksPort.  If
  // talking to a system tor, this may be an AF_UNIX socket, or
  // 127.0.0.1:9050 instead.
  tbProxyURL, err := url.Parse(proxyURI)
  if err != nil {
    fatalf("Failed to parse proxy URL: %v\n", err)
  }

  // Get a proxy Dialer that will create the connection on our
  // behalf via the SOCKS5 proxy.  Specify the authentication
  // and re-create the dialer/transport/client if tor's
  // IsolateSOCKSAuth is needed.
  tbDialer, err := proxy.FromURL(tbProxyURL, proxy.Direct)
  if err != nil {
    fatalf("Failed to obtain proxy dialer: %v\n", err)
  }

  // Make a http.Transport that uses the proxy dialer, and a
  // http.Client that uses the transport.
  tbTransport := &http.Transport{Dial: tbDialer.Dial}
  client := &http.Client{Transport: tbTransport}

  return &MyHttpClient{client, map[string]string{}}
}

func (self *MyHttpClient) AddHeader(key string, value string) {
  self.headers[key] = value
}

func (self *MyHttpClient) GET(uri string, params map[string]string) (string, error) {
  result := ""
  // Example: Fetch something.  Real code will probably want to use
  // client.Do() so they can change the User-Agent.
  url, err := getFullURL(uri, params);

  if err != nil {
    return result, err
  }

  req, err := http.NewRequest("GET", url, nil)
  if err != nil {
   return result, err
  }

  for key, value := range self.headers {
   req.Header.Set(key, value)
  }

  resp, err := self.client.Do(req)
  if err != nil {
   return result, err
  }
  defer resp.Body.Close()

  body, err := ioutil.ReadAll(resp.Body)
  if err != nil {
   return result, err
  }

  return string(body), nil
}

func (self *MyHttpClient) GetMyIP() (string, bool) {
  tor := false
  body, err := self.GET("https://check.torproject.org", map[string]string{})
  if err != nil {
    return fmt.Sprintf("Failed to read the address: %v\n", err), tor
  }
  if (strings.Contains(body, "Congratulations. This browser is configured to use Tor.")) {
    tor = true
  }
  var re = regexp.MustCompile(`to be:  <strong>([0-9.]+)<`)
  for _, match := range re. FindAllStringSubmatch(body, -1) {
      return match[1], tor
  }
  return "Unknown", tor
}

func getFullURL(uri string, args map[string]string) (string, error) {
  baseUrl, err := url.Parse(uri)
  	if err != nil {
  		return "", err
  	}

  	params := url.Values{}
    for key, value := range args {
  	   params.Add(key, value)
    }

  	baseUrl.RawQuery = params.Encode()
  	return string(baseUrl.String()), nil
}
